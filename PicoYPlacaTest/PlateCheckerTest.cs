﻿using PicoYPlaca;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PicoYPlacaTest
{
    /// <summary>
    ///This is a test class for LicenseCheckerTest and is intended
    ///to contain all LicenseCheckerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PlateCheckerTest
    {
        /// <summary>
        ///A test for isValidDateTime
        ///</summary>
        ///Testing a valid DateTime, True is expected
        [TestMethod()]
        public void validDateTimeTest()
        {
            var checker = new PlateChecker("N/A");

            Assert.IsTrue(checker.isValidDateAndTime("12/04/2018","17:00"));
        }

        /// <summary>
        ///A test for isValidDateTime
        ///</summary>
        ///Testing a invalid DateTime, False is expected
        [TestMethod()]
        public void invalidDateTimeTest()
        {
            var checker = new PlateChecker("N/A");

            Assert.IsFalse(checker.isValidDateAndTime("12/04/2018", "1700"));
        }

        /////////////////////////////////
        // Testing Valid Plate Numbers //
        /////////////////////////////////

        /// <summary>
        ///A test for checkPlate
        ///</summary>
        ///Testing a Monday Plate Check in the Morning, True is expected
        [TestMethod()]
        public void checkMorningTrueTest()
        {
            var checker = new PlateChecker("PCX-2991");

            Assert.IsTrue(checker.checkPlate("19/03/2018", "06:23"));
        }

        /// <summary>
        ///A test for checkPlate
        ///</summary>
        ///Testing a Monday Plate Check in the Morning, False is expected
        [TestMethod()]
        public void checkMorningFalseTest()
        {
            var checker = new PlateChecker("A57-B32N");

            Assert.IsFalse(checker.checkPlate("06/11/2017", "08:54"));
        }

        /// <summary>
        ///A test for checkPlate
        ///</summary>
        ///Testing a Thursday Plate Check in the Night, True is expected
        [TestMethod()]
        public void checkNightTrueTest()
        {
            var checker = new PlateChecker("PCH-108");

            Assert.IsTrue(checker.checkPlate("07/11/2017", "17:53"));
        }

        /// <summary>
        ///A test for checkPlate
        ///</summary>
        ///Testing a Wednesday Plate Check in the Night, False is expected
        [TestMethod()]
        public void checkNightFalseTest()
        {
            var checker = new PlateChecker("PCH-775");

            Assert.IsFalse(checker.checkPlate("10/01/2018", "17:03"));
        }

        //////////////////////
        // Testing Weekends //
        //////////////////////

        /// <summary>
        ///A test for checkPlate
        ///</summary>
        ///Testing a Saturday Plate Check in the Morning, True is expected (All Saturday days are always True)
        [TestMethod()]
        public void checkWeekendMorningTest()
        {
            var checker = new PlateChecker("PCH-108");

            Assert.AreEqual(true,checker.checkPlate("07/04/2018", "10:53"));
        }

        /// <summary>
        ///A test for checkPlate
        ///</summary>
        ///Testing a Sunday Plate Check in the Morning, True is expected (All Sunday days are always True)
        [TestMethod()]
        public void checkWeekendNightTest()
        {
            var checker = new PlateChecker("PCX-75R");

            Assert.AreNotEqual(false,checker.checkPlate("24/12/2018", "18:59"));
        }
    }
}
