﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PicoYPlaca
{
    /// <summary>
    /// Class for check the times
    /// </summary>
    /// <remarks>
    /// This class identify the day of week and checks using a Date specified
    /// </remarks>
    class DateCheck : TimeCheck
    {
        private DateTime theDate;

        /// <summary>
        /// Constructor using a Datetime type
        /// </summary>
        /// <remarks>
        /// @param name="date": The date to assign
        /// </remarks>
        public DateCheck(DateTime date)
        {
            this.theDate = date;
        }
        /// <summary>
        /// Obtains the day of the week from the property theDate
        /// </summary>
        /// <remarks>
        /// @returns the day of the week as int value
        /// </remarks>
        private int getDayOfWeek()
        {
            return Convert.ToInt32(this.theDate.DayOfWeek);
        }
        /// <summary>
        /// Checks the date from the property theDate considering the digit specified
        /// </summary>
        /// <remarks>
        /// @param name="digit": The digit to check with the property theDate
        /// @returns True if the conditions not applied to the digit and the property theDate, or depends on the
        /// checkHour call of the inherited class
        /// </remarks>
        public bool checkDate(int digit)
        {
            int newDigit = (digit.Equals(0)) ? 10 : digit;
            int dayOfWeekCalcultated = (newDigit % 2) + (newDigit / 2); //This Formula determine the day of the week according to the rules for Pico y Placa

            if (dayOfWeekCalcultated.Equals(getDayOfWeek()))
            {
                return checkHour(Convert.ToInt32(this.theDate.ToString("HHmm")));
            }
            return true;
        }
        /// <summary>
        /// Sets the property theDate value
        /// </summary>
        /// <remarks>
        /// @param name="date": the date value to be assign
        /// </remarks>
        public void setDate(DateTime date)
        {
            this.theDate = date;
        }
        /// <summary>
        /// Gets the property theDate value
        /// </summary>
        /// <remarks>
        /// @returns the date value stored in the property theDate
        /// </remarks>
        public DateTime getDate()
        {
            return this.theDate;
        }
    }
}
