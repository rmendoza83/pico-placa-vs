﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Linq;

namespace PicoYPlaca
{
    /// <summary>
    /// Class for checks the plates of the vehicles
    /// </summary>
    /// <remarks>
    /// This class determine if a vehicle can or not be moving according to the Date, Time and Plate Number specified
    /// </remarks>
    public class PlateChecker
    {
        private DateCheck dateCalc;
        private string plateNumber;

        /// <summary>
        /// Constructor using a string plate number
        /// </summary>
        /// <remarks>
        /// @param name="plateNumber": the plate number to be assign to the class
        /// </remarks>
        public PlateChecker(string plateNumber)
        {
            this.plateNumber = plateNumber;
            dateCalc = new DateCheck(DateTime.Now);
        }
        /// <summary>
        /// Obtains the last digit of the plate number
        /// </summary>
        /// <remarks>
        /// @returns the last digit of the plate number as character
        /// </remarks>
        private Char getLastDigit()
        {
            return plateNumber.Last(Char.IsDigit);
        }
        /// <summary>
        /// Converts string date to Datetime date according to the culture of Ecuador
        /// </summary>
        /// <remarks>
        /// @param name="dateString": the date to be parsed
        /// @returns the date as DateTime object with the culture especified
        /// </remarks>
        public DateTime parseDateTime(string dateString)
        {
            string format = "dd/MM/yyyy HH:mm";
            CultureInfo culture = new CultureInfo("es-EC");
            return DateTime.ParseExact(dateString, format, culture);
        }
        /// <summary>
        /// Checks the date and time strings are valid to be parsed
        /// </summary>
        /// <remarks>
        /// @param name="date": the date to checks
        /// @param name="time": the time to checks
        /// @returns True if the both strings are valid and can be parsed, otherwise False
        /// </remarks>
        public bool isValidDateAndTime(string date, string time)
        {
            try
            {   
                parseDateTime(String.Format("{0} {1}",date,time));
                return true;
            }
            catch (FormatException)
            {
            }
            return false;
        }
        /// <summary>
        /// Checks the plate number, using the date and time specified
        /// </summary>
        /// <remarks>
        /// @param name="date": the date to checks
        /// @param name="time": the time to checks
        /// @returns True if the vehicle maybe moving, otherwise False if the vehicle can't be moving
        /// </remarks>
        public bool checkPlate(string date, string time)
        {
            if (isValidDateAndTime(date,time))
            {
                dateCalc.setDate(parseDateTime(String.Format("{0} {1}",date,time)));
                int digit = (int)Char.GetNumericValue(getLastDigit());
                return dateCalc.checkDate(digit);
            }
            return true;
        }
    }
}
