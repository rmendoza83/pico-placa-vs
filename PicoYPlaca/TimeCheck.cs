﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PicoYPlaca
{
    /// <summary>
    /// Class for check the times
    /// </summary>
    /// <remarks>
    /// This class determine the range of hours about the Pico and Placa
    /// </remarks>
    class TimeCheck
    {
        private readonly int HourA1 = 700; //07:00
        private readonly int HourA2 = 930; //09:30
        private readonly int HourB1 = 1600; //16:00
        private readonly int HourB2 = 1930; //19:30

        /// <summary>
        /// Checks the hour specified
        /// </summary>
        /// <remarks>
        /// @param name="Hour": The Hour to check
        /// @returns True if the vehicle maybe moving, otherwise False if the vehicle can't be moving
        /// </remarks>
        public bool checkHour(int Hour)
        {
            return !((Hour >= HourA1 && Hour <= HourA2) || (Hour >= HourB1 && Hour <= HourB2));
        }
    }
}
