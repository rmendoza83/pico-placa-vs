﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PicoYPlaca
{
    /// <summary>
    /// Class Main Program
    /// </summary>
    /// <remarks>
    /// Main Program entry point
    /// </remarks>
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length.Equals(3))
            {
                PlateChecker plateChecker = new PlateChecker(args[0]);
                if (!plateChecker.isValidDateAndTime(args[1], args[2]))
                {
                    Console.WriteLine("Invalid Date and Time params...");
                }
                if (plateChecker.checkPlate(args[1], args[2]))
                {
                    Console.WriteLine("The Car can be in the road");
                }
                else
                {
                    Console.WriteLine("The Car can't be in the road");
                }
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Invalid Input params...");
            }
        }
    }
}
